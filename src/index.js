// edit CONFIG from config.js

var app = Vue.createApp({
    data() {
        return {
            slides: [],
            index: 0,
            timeout_handler: null,
        };
    },
    mounted() {
        // Initialize slides from the config file
        this.slides = CONFIG.SLIDES.map((s) => ({
            url: s.URL,
            duration: s?.DURATION || CONFIG.BASE_SLIDE_DURATION, // if no duration specified, we apply the base one
        }));

        // Initialize the recursive update loop
        this.loopNext();
    },
    beforeDestroy() {
        clearTimeout(this.timeout_handler);
    },
    methods: {
        isCurrent(index) {
            return this.index == index;
        },
        isPreRenderedElement(index) {
            return this.index == (index + 1) % this.slides.length;
        },
        isRendered(index) {
            return this.isCurrent(index) || this.isPreRenderedElement(index);
        },
        isVisible(index) {
            return this.isCurrent(index);
        },
        goNext() {
            this.index = (this.index + 1) % this.slides.length;
        },
        loopNext() {
            this.timeout_handler = setTimeout(() => {
                this.goNext(); // Go to the next slide pannel
                this.loopNext(); // Recursive call
            }, this.slides[this.index].duration); // Inject the current slide duration
        },
    },
});

app.mount("#app");
