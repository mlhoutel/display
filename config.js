const CONFIG = {
    BASE_SLIDE_DURATION: 5000,
    SLIDES: [
        {
            URL: "slides/vistar.html",
            DURATION: 20000,
        },
        {
            URL: "http://ab-atb-lpe.web.cern.ch/ab-atb-lpe/piquet/2.0/?fixedDisplay=true&subsystem=collimators&subsystem=",
            DURATION: 10000,
        },
    ],
};
