#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

function die() {
    echo "Error: $1" >&2
    exit 1
}

echo "Cleaning dist directory"
rm -rf dist
mkdir dist

echo "Generating site"
pug src/index.pug || die 'Failed to generate site'
mv src/index.html dist
cp -r static/* dist

