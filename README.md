# Display

This is a simple webite made with [pug](https://pugjs.org/) and [vue3](https://vuejs.org/) to display a list of slides in the form of a carousel.

## 1. Getting started

### 1.1. Setup

1. Clone this project with `git clone https://gitlab.cern.ch/mlhoutel/display.git`
2. Install [nodejs](https://nodejs.org/en/) (`v16+` recommanded)
3. Setup with `npm install`

### 1.2. Run

```bash
npm run dev     # hot reload build with nodemon
npm run serve   # expose the build on http-server
npm run build   # build and reload the dist folder
```

## 2. Edit

### 2.1. Displayed frames

You can change the order of the displayed frames, or even replace them with your own:

#### 2.1.1. Remote slides

1. Make sure that the resources to be displayed are `available via a url`, and that their headers do not contain the [X-Frame-Options](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options) property. 
2. Place the urls to the assets to be displayed (web site, images, pdf...) in the `config.js` file as such

```js
SLIDES: [
    {
        URL: "{slide_url}.html", // the url to the slide asset 
        DURATION: 5000,          // (optional) the slide duration in ms
    },
]
```

if the `DURATION` property is not specified for a given slide, the `BASE_SLIDE_DURATION` will ne applied from the `config.js` file:

```js
BASE_SLIDE_DURATION: 10000, // default slide duration in ms
```

#### 2.1.2. Local slides

1. Place the assets to be displayed (web site, images, pdf...) in the `static/carousel` folder.
2. Place the relative urls to the assets to be displayed in the `config.js` file as such:

```js
SLIDES: [
    {
        URL: "slides/{slide_source}.html", // the url to the slide asset
        DURATION: 20000,                   // (optional) the slide duration in ms
    },
]
```

You can then run the `build.sh` script, that will compile the `index.pug` file and it's dependancies in `index.html`, and copy it and the static ressources in the `dist` folder.

### 2.2. Delay and animations

You can also customise the duration and the style of the animations:

1. Edit the duration of the animation by changing the `:duration` property of the `transition-group` in the `src/style.pug` file (in ms)
2. Modify the `transition` duration in the `.list-enter-active` and `.list-leave-active` classes in `src/style.css`. 
